import React from "react";
import Footer from "../components/footer/Footer";
import Header from "../components/Header/Header";

export default function Layout({ children }) {
  return (
    <div>
      <Header />
      {children}
      <Footer />
    </div>
  );
}
