import React, { useEffect, useState } from "react";
import { getMovie } from "../service/movieService";
import ListMovie from "./ListMovie";
import MovieTab from "./MovieTab/MovieTab";
export default function HomePage() {
  let [movie, setMovie] = useState([]);
  useEffect(() => {
    getMovie()
      .then((res) => {
        console.log(res);
        setMovie(res.data.content);
      })
      .catch((err) => {
        console.log(err);
      });
  }, []);

  return (
    <div>
      <ListMovie movie={movie} />
      <MovieTab />
    </div>
  );
}
