import React from "react";
import moment from "moment";
import "moment/locale/vi";
moment.locale("vi");
export default function MovieItemTab({ movie }) {
  return (
    <div className="flex mt-10 space-x-5">
      <img className="h-40 w-28 object-cover" src={movie.hinhAnh} alt="" />
      <div>
        <h3 className="font-medium text-2xl text-left">{movie.tenPhim}</h3>
        <div className="grid grid-cols-3 gap-7 mt-5">
          {movie.lstLichChieuTheoPhim.slice(0, 6).map((lichChieu) => {
            console.log("movie", movie);
            // console.log("lichChieu", lichChieu);

            return (
              <p className="bg-red-500 text-white px-3 py-2 rounded">
                {moment(lichChieu.ngayChieuGioChieu).format("lll")}
              </p>
            );
          })}
        </div>
      </div>
    </div>
  );
}

//     "lstLichChieuTheoPhim": [
//         {
//             "maLichChieu": 45606,
//             "maRap": "780",
//             "tenRap": "Rạp 10",
//             "ngayChieuGioChieu": "2022-10-20T16:10:08",
//             "giaVe": 75000
//         }
//     ],
//     "maPhim": 1438,
//     "tenPhim": "Mad Max Road",
//     "hinhAnh": "https://movienew.cybersoft.edu.vn/hinhanh/mad-max-rooaadd_gp01.jpg",
//     "hot": true,
//     "dangChieu": true,
//     "sapChieu": true
// }
