import React, { useEffect, useState } from "react";
import { getMovieByTheater } from "../../service/movieService";
import { Tabs } from "antd";
import MovieItemTab from "./MovieItemTab";

export default function MovieTab() {
  let [dataMovie, setDataMovie] = useState([]);
  console.log("dataMovie", dataMovie);
  useEffect(() => {
    getMovieByTheater()
      .then((res) => {
        console.log("getMovieByTheater", res);
        setDataMovie(res.data.content);
      })
      .catch((err) => {
        console.log(err);
      });
  }, []);

  const onChange = (key) => {
    console.log(key);
  };
  // const renderTen = (item) => {
  //   return item.lstCumRap.map((item) => {
  //     return (
  //       <>
  //         {item.tenCumRap}
  //         {item.diaChi}
  //       </>
  //     );
  //   });
  // };
  const renderHeThongRap = () => {
    return dataMovie.map((heThongRap) => {
      // console.log("heThongRap", heThongRap);
      return {
        label: <img className="w-16 h-16" src={heThongRap.logo} alt="" />,
        key: heThongRap.maHeThongRap,
        children: (
          <Tabs
            style={{
              height: 400,
            }}
            tabPosition="left"
            defaultActiveKey="1"
            onChange={onChange}
            items={renderCumRapTheoHeThongRap(heThongRap)}
          />
        ),
      };
    });
  };
  const renderCumRapTheoHeThongRap = (heThongRap) => {
    return heThongRap.lstCumRap.map((cumRap) => {
      // console.log("cumRap", cumRap);
      return {
        label: (
          <div className="w-44">
            <h5 className="truncate">{cumRap.tenCumRap}</h5>
            <p className="truncate">{cumRap.diaChi}</p>
          </div>
        ),
        key: cumRap.tenCumRap,
        children: (
          <div style={{ height: 400, overflowY: "scroll" }}>
            {renderDanhSachPhimTheoCumRap(cumRap)}
          </div>
        ),
      };
    });
  };
  const renderDanhSachPhimTheoCumRap = (cumRap) => {
    return cumRap.danhSachPhim.map((movie) => {
      // console.log("movie", movie);
      return <div>{<MovieItemTab movie={movie} />}</div>;
    });
  };
  return (
    <div className="container mx-auto mt-20">
      {" "}
      <Tabs
        style={{
          //////////////////////
          margin: "auto",
          width: 1200,
          height: 400,
        }}
        tabPosition="left"
        defaultActiveKey="1"
        onChange={onChange}
        items={renderHeThongRap()}
      />
    </div>
  );
}
