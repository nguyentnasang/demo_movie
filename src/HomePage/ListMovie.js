import React from "react";
import { Card } from "antd";

import { NavLink } from "react-router-dom";
const { Meta } = Card;
export default function ListMovie({ movie }) {
  let renderMovie = () => {
    return movie.slice(0, 10).map((item) => {
      return (
        <Card
          className="border-gray-600"
          hoverable
          style={{
            width: "100%",
          }}
          cover={
            <img
              className="h-96 object-cover"
              alt="example"
              src={item.hinhAnh}
            />
          }
        >
          <Meta className="h-14" title={item.tenPhim} />
          <NavLink
            to={`/detail/${item.maPhim}`}
            className="bg-red-300 px-3 py-2"
          >
            Xem chi tiet
          </NavLink>
        </Card>
      );
    });
  };
  return (
    <div className="grid grid-cols-1 md:grid-cols-3 lg:grid-cols-5 gap-6 p-9">
      {renderMovie()}
    </div>
  );
}
