import React from "react";
import { Desktop, Mobile, Tablet } from "../../HOC/Reponsive";
import FooterDesktop from "./FooterDesktop";
import FooterTablet from "./FooterTablet";
import FooterMobile from "./FooterMobile";

export default function Footer() {
  return (
    <div>
      <Desktop>
        <FooterDesktop />
      </Desktop>
      <Tablet>
        <FooterTablet />
      </Tablet>
      <Mobile>
        <FooterMobile></FooterMobile>
      </Mobile>
    </div>
  );
}
