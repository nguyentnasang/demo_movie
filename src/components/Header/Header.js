import React from "react";
import { useSelector } from "react-redux";
import { userLocalService } from "../../service/localService";
import { NavLink } from "react-router-dom";

export default function Header() {
  let user = useSelector((state) => {
    console.log("state", state);
    return state.userReducer.user;
  });
  userLocalService.set(user);
  const renderLogout = () => {
    userLocalService.remove();
    window.location.href = "/";
  };
  const renderUser = () => {
    if (user) {
      return (
        <>
          <p>ten dang nhap: {user.hoTen}</p>

          <button
            onClick={() => {
              renderLogout();
            }}
            className="border-2 border-black px-5 py-2 rounded "
          >
            đăng xuất
          </button>
        </>
      );
    } else {
      return (
        <>
          <NavLink to={"/login"}>
            <button className="border-2 border-black px-5 py-2 rounded ">
              đăng nhập
            </button>
          </NavLink>
          <button className="border-2 border-black px-5 py-2 rounded ">
            đăng ký
          </button>
        </>
      );
    }
  };
  return (
    <div className="bg-red-300 py-10 flex justify-between px-40">
      <p>MOVIE</p>
      <p className="flex space-x-5 justify-center items-center">
        {renderUser()}
      </p>
    </div>
  );
}
