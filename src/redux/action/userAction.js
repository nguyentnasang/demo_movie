import { USER_REDUCER } from "./../contant/contant";
import { postLogin } from "./../../service/userService";
import { message } from "antd";
// export const setUserAction = (value) => {
//   return { type: USER_REDUCER, payload: value };
// };
export const setUserActionService = (value, onSuccess) => {
  return (dispatch) => {
    postLogin(value)
      .then((res) => {
        message.success("dangnhapthanhcong");
        dispatch({
          type: USER_REDUCER,
          payload: res.data.content,
        });
        onSuccess();
        console.log(res);
      })
      .catch((err) => {
        message.success("dangnhapthatbai");

        console.log(err);
      });
  };
};
