import { userLocalService } from "../../service/localService";
import { USER_REDUCER } from "./../contant/contant";
const initialState = {
  user: userLocalService.get(),
};

export const userReducer = (state = initialState, { type, payload }) => {
  switch (type) {
    case USER_REDUCER: {
      return { ...state, user: payload };
    }
    default:
      return state;
  }
};
