import { https } from "./configURL";
export const getMovie = () => {
  return https.get("/api/QuanLyPhim/LayDanhSachPhim");
};
export const getMovieByTheater = () => {
  return https.get("/api/QuanLyRap/LayThongTinLichChieuHeThongRap");
};
