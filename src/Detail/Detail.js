import React from "react";
import { useParams } from "react-router-dom";

export default function Detail() {
  let param = useParams();
  console.log("param", param);
  return <div>Ma phim: {param.id}</div>;
}
