import React from "react";
import { Button, Form, Input } from "antd";
// import { postLogin } from "./../service/userService";
import { useNavigate } from "react-router-dom";
import { useDispatch } from "react-redux";
// import { USER_REDUCER } from "../redux/contant/contant";
import Lottie from "lottie-react";
import bg_animate from "../assets/data.json";
import { setUserActionService } from "../redux/action/userAction";
export default function LoginPage() {
  let dispatch = useDispatch();
  const navigate = useNavigate();
  // const onFinish = (values) => {
  //   console.log("Success:", values);
  //   postLogin(values)
  //     .then((res) => {
  //       console.log(res);
  //       dispatch({ type: USER_REDUCER, payload: res.data.content });

  //       message.success("dangnhapthanhcong");

  //       setTimeout(() => {
  //         navigate("/");
  //       }, 1000);
  //     })
  //     .catch((err) => {
  //       console.log(err);
  //       message.success("dangnhapthatbai");
  //     });
  // };
  const onFinishReduxThunk = (values) => {
    const handleNavigate = () => {
      setTimeout(() => {
        navigate("/");
      }, 1000);
    };
    dispatch(setUserActionService(values, handleNavigate));
    console.log("values", values);
  };
  const onFinishFailed = (errorInfo) => {
    console.log("Failed:", errorInfo);
  };
  return (
    <div className="h-screen w-screen bg-pink-400 flex justify-center items-center">
      <div className="w-1/2">
        <Lottie animationData={bg_animate} loop={true} />;
      </div>
      <div className="w-1/2">
        <Form
          className="bg-pink-300 rounded p-5"
          layout="vertical"
          name="basic"
          labelCol={{
            span: 8,
          }}
          wrapperCol={{
            span: 24,
          }}
          initialValues={{
            remember: true,
          }}
          onFinish={onFinishReduxThunk}
          onFinishFailed={onFinishFailed}
          autoComplete="off"
        >
          <Form.Item
            label="Username"
            name="taiKhoan"
            rules={[
              {
                required: true,
                message: "Please input your username!",
              },
            ]}
          >
            <Input />
          </Form.Item>

          <Form.Item
            label="Password"
            name="matKhau"
            rules={[
              {
                required: true,
                message: "Please input your password!",
              },
            ]}
          >
            <Input.Password />
          </Form.Item>

          <Form.Item
            name="remember"
            valuePropName="checked"
            wrapperCol={{
              offset: 8,
              span: 16,
            }}
          ></Form.Item>

          <Form.Item
            className="flex items-center justify-center"
            wrapperCol={{
              offset: 8,
              span: 16,
            }}
          >
            <Button className="bg-yellow-400" htmlType="submit">
              Submit
            </Button>
          </Form.Item>
        </Form>
      </div>
    </div>
  );
}
